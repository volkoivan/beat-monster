﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Command : MonoBehaviour
{
    public bool enemy = false;
    public BeatType beatType;
    public bool isTarget = false;
    public bool isState = false;
    public BoxCollider2D boxCollider;
    private Vector3 screenPoint;
    private Vector3 offset;
    private SpriteRenderer spriteRenderer;
    public GameObject stateContainer;
    public Sprite spawnSprite;
    public Sprite moveSprite;
    public Sprite attackSprite;
    public Sprite enemyAttackSprite;
    public Sprite healSprite;
    public Sprite getCoinsSprite;
    public Sprite emptySprite;
    public Sprite getAmmoSprite;
    public Sprite command0ContainerSprite;
    public Sprite command1ContainerSprite;

    private void Awake() {
        boxCollider = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (beatType) {
            case BeatType.GetAmmo:
                spriteRenderer.sprite = getAmmoSprite;
                break;
            case BeatType.Move:
                spriteRenderer.sprite = moveSprite;
                break;
            case BeatType.GetCoins:
                spriteRenderer.sprite = getCoinsSprite;
                break;
            case BeatType.EnemyAttack:
                spriteRenderer.sprite = enemyAttackSprite;
                break;
            case BeatType.Attack:
                spriteRenderer.sprite = attackSprite;
                break;
            case BeatType.Empty:
                spriteRenderer.sprite = null;
                break;
            case BeatType.Heal:
                spriteRenderer.sprite = healSprite;
                break;
            case BeatType.Spawn:
                spriteRenderer.sprite = spawnSprite;
                break;
        }
        if (!isTarget && !isState) {
            spriteRenderer.color = CanSelect() ? Color.white : Color.gray;
        }
    }

    bool CanSelect() {
        return true;//beatType != BeatManager.instance.playerCommand0.beatType && beatType != BeatManager.instance.playerCommand1.beatType;
    }

    public void Setup(BeatType beatType, bool active, int commandType) {
        this.beatType = beatType;
        stateContainer.GetComponent<SpriteRenderer>().sprite = commandType == 0 ? command0ContainerSprite : command1ContainerSprite;
        if (stateContainer != null) {
            if (enemy) {
                stateContainer.GetComponent<SpriteRenderer>().color = active ? Color.red : Color.gray;
            } else {
                stateContainer.GetComponent<SpriteRenderer>().color = active ? Color.white : Color.gray;
            }
        }
    }


    void OnMouseDown()
    {
        if (!isTarget && !isState && CanSelect()) {
            AudioManager.instance.UIClick();
            Instantiate(this, this.transform.parent);
            var screenPoint = Camera.main.WorldToScreenPoint(transform.position);
            var offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,screenPoint.z));
        }
    }

    void OnMouseDrag()
    {
        if (!isTarget && !isState && CanSelect()) {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            transform.position = new Vector3(curPosition.x, curPosition.y, 0);
        }
    }

    private void OnMouseUp() {
        if (!isTarget && !isState && CanSelect()) {
            AudioManager.instance.UIClick();
            var target0Distance = transform.position - BeatManager.instance.playerCommand0.transform.position;
            var target1Distance = transform.position - BeatManager.instance.playerCommand1.transform.position;
            if (Mathf.Abs(target0Distance.x) <= 2 && Mathf.Abs(target0Distance.y) <= 2) {
                BeatManager.instance.playerCommand0.beatType = this.beatType;
            } else if (Mathf.Abs(target1Distance.x) <= 2 && Mathf.Abs(target1Distance.y) <= 2) {
                BeatManager.instance.playerCommand1.beatType = this.beatType;
            }
            Destroy(gameObject);
        }
    }
}
