﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy : MonoBehaviour
{
    public int moveStep = 100;
    public int maxHealth = 1;
    public int health = 1;
    public int damage = 1;
    private Rigidbody2D rb;
    [SerializeField]
    private GameObject twoHearts;
    [SerializeField]
    private GameObject oneHeart;
    [SerializeField]
    private GameObject attackSprite;
    public ObjectEvent enemyDied = new ObjectEvent();
    [SerializeField]
    private GameObject targetMark;
    [SerializeField]
    private GameObject deathParticles;
    
    // Start is called before the first frame update

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        UpdateHealth();
    }

    void UpdateHealth() {

        twoHearts.SetActive(health == 2);
        oneHeart.SetActive(health == 1);
    }

    // Update is called once per frame
    void Update()
    {
        var currentEnemies = GameManager.instance.enemies.GetRange(0, UpgradeManager.instance.shotArrowsPerBeat);
        targetMark.SetActive(currentEnemies.Contains(gameObject));
    }

    public void Beat(BeatType type) {
        switch (type)
        {
            case BeatType.Move:
                var player = GameObject.FindObjectOfType<Player>();
                var playerPos = player.gameObject.transform.position;
                var direction = playerPos - transform.position;
                var directionNormalized = direction.normalized;
                var distance = Vector2.Distance(playerPos, transform.position);
                if (distance <= 1) {
                    player.DoDamage(this);
                    var attack = Instantiate(attackSprite, transform.parent);
                    attack.transform.position = transform.position + directionNormalized;
                    Destroy(attack, 1);
                } else {
                    var moveX = Random.Range(0, 2) == 0;
                    if (Mathf.Abs(directionNormalized.y) <= 0.1) {
                        moveX = false;
                    } else if (Mathf.Abs(directionNormalized.x) <= 0.1) {
                        moveX = true;
                    }
                    rb.AddForce(new Vector2(moveX ? (directionNormalized.x < 0 ? -1 : 1) : 0, moveX ? 0 : (directionNormalized.y < 0 ? -1 : 1))*moveStep);
                }
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("Arrow")) {
            health -= 1;
            UpdateHealth();
            if (health <= 0) {
                var particles = Instantiate(deathParticles, transform.parent);
                particles.transform.position = transform.position;
                Destroy(particles, 2f);
                Destroy(gameObject);
                enemyDied.Invoke(gameObject);
            }
        }
    }
}
