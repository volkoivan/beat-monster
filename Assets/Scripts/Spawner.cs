﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    static public Spawner instance;
    [SerializeField]
    private GameObject[] enemyPrefabs;
    [SerializeField]
    private GameObject[] portals;

    private void Awake() {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Spawn() {
        var maxIndex = Mathf.Min(GameManager.instance.level, enemyPrefabs.Length);
        var count = Mathf.Min(4, Mathf.Max(GameManager.instance.level / 2, 1));
        for (int i =0; i < count; i++) {
            var enemyObj = Instantiate(enemyPrefabs[Random.Range(0, maxIndex)], transform.parent);
            enemyObj.transform.position = portals[Random.Range(0, portals.Length)].transform.position;
            GameManager.instance.AddEnemy(enemyObj);
        }
    }
}
