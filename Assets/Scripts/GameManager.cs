﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;


public class ObjectEvent: UnityEvent<GameObject> {
    
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField]
    private VolumeProfile volumeProfile;
    private ChromaticAberration aberration; 
    public int diamonds = 0;
    [SerializeField]
    public Player player;
    public List<GameObject> enemies;
    public int beatsFinished = 0;
    public int level = 0;
    

    private void Awake() {
        volumeProfile.TryGet<ChromaticAberration>(out aberration);
        GameManager.instance = this;
    }

    void Start()
    {
        BeatManager.instance.onBeatFinished.AddListener(BeatFinished);
        player.playerDied.AddListener(EndGame);
    }

    void BeatFinished() {
        beatsFinished += 1;
        level = (int)beatsFinished/3;
        Debug.Log(level);
    }

    void EndGame() {
        var previousScore = PlayerPrefs.GetInt("highScore", 0);
        if (previousScore < beatsFinished) {
            PlayerPrefs.SetInt("highScore", beatsFinished);
        }
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) {
            enemies.Remove(enemy);
            Destroy(enemy);
        }
        foreach (GameObject arrow in GameObject.FindGameObjectsWithTag("Arrow")) {
            Destroy(arrow);
        }
        beatsFinished = 0;
        diamonds = 0;
        StateManager.instance.EndGame();
        UpgradeManager.instance.Reset();
        player.arrowsCount = 20;
        player.health = UpgradeManager.instance.maxHealth;
    }

    private void Update() {
        aberration.intensity.value = aberration.intensity.value - Time.deltaTime*2f;
        aberration.intensity.value = Mathf.Max(0, aberration.intensity.value);
    }

    public void Beat() {
        aberration.intensity.value = Random.Range(0.4f, 0.6f);
    }

    void FixedUpdate()
    {
    }

    public void AddEnemy(GameObject enemy) {
        enemy.GetComponent<Enemy>().enemyDied.AddListener(GameManager.instance.EnemyDied);
        enemies.Add(enemy);
        enemies.Sort(delegate(GameObject a, GameObject b) 
        {
            var distance1 = Vector2.Distance(a.transform.position, player.transform.position);
            var distance2 = Vector2.Distance(b.transform.position, player.transform.position);
            return distance1.CompareTo(distance2);
        });
    }

    public void EnemyDied(GameObject enemy) {
        enemies.Remove(enemy);
    }
}
