﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    static public UIManager instance;
    [SerializeField]
    private Text beatsText;
    [SerializeField]
    private Text diamondsCountText;
    [SerializeField]
    private Text highScoreText;
    [SerializeField]
    private Text arrowsCountText;
    [SerializeField]
    private Sprite fullHeartSprite;
    [SerializeField]
    private Sprite halfHeartSprite;
    [SerializeField]
    private Sprite emptyHeartSprite;
    [SerializeField]
    private SpriteRenderer[] hearts;
    public Command[] enemyCommands;
    public Command[] playerCommands;
    [SerializeField]
    private Button beatButton;
    [SerializeField]
    private Button playButton;
    [SerializeField]
    private Text upgradeHealthText;
    [SerializeField]
    private Text upgradeArrowsText;
    [SerializeField]
    private Text upgradeBowText;
    [SerializeField]
    private Button upgradeHealthButton;
    [SerializeField]
    private Button upgradeArrowsButton;
    [SerializeField]
    private Button upgradeBowButton;

    private void Awake() {
        UIManager.instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        beatButton.onClick.AddListener(StartBeat);
        playButton.onClick.AddListener(StartGame);
        upgradeBowButton.onClick.AddListener(UpgradeBow);
        upgradeArrowsButton.onClick.AddListener(UpgradeArrows);
        upgradeHealthButton.onClick.AddListener(UpgradeHealth);
        UpdateUpgrades();
    }
    public void UpgradeBow() {
        AudioManager.instance.UIClick();
        UpgradeManager.instance.UpgradeBow();
    }

    public void UpgradeArrows() {
        AudioManager.instance.UIClick();
        UpgradeManager.instance.UpgradeArrows();
    }

    public void UpgradeHealth() {
        AudioManager.instance.UIClick();
        UpgradeManager.instance.UpgradeHealth();
    }

    void StartBeat() {
        AudioManager.instance.UIClick();
        StateManager.instance.StartBeat();
    }

    void StartGame() {
        AudioManager.instance.UIClick();
        StateManager.instance.StartGame();
    }

    public void UpdateUpgrades() {
        upgradeArrowsText.text = UpgradeManager.instance.arrowsPrice.ToString();
        upgradeBowText.text = UpgradeManager.instance.bowPrice.ToString();
        upgradeHealthText.text = UpgradeManager.instance.healthPrice.ToString();
        upgradeArrowsButton.gameObject.SetActive(UpgradeManager.instance.maxLevel > UpgradeManager.instance.arrowsLevel);
        upgradeBowButton.gameObject.SetActive(UpgradeManager.instance.maxLevel > UpgradeManager.instance.bowLevel);
        upgradeHealthButton.gameObject.SetActive(UpgradeManager.instance.maxLevel > UpgradeManager.instance.healthLevel);
        upgradeArrowsButton.enabled = GameManager.instance.diamonds >= UpgradeManager.instance.arrowsPrice;
        upgradeBowButton.enabled = GameManager.instance.diamonds >= UpgradeManager.instance.bowPrice;
        upgradeHealthButton.enabled = GameManager.instance.diamonds >= UpgradeManager.instance.healthPrice;
    }

    // Update is called once per frame
    void Update()
    {
        var highScore = PlayerPrefs.GetInt("highScore", 0);
        highScoreText.text = "HIGH SCORE\n" + highScore.ToString() + " BEATS";
        arrowsCountText.text = GameManager.instance.player.GetComponent<Player>().arrowsCount.ToString();
        diamondsCountText.text = GameManager.instance.diamonds.ToString();
        for (int i = 0; i<hearts.Length; i++) {
            var currentHealth = GameManager.instance.player.GetComponent<Player>().health;
            var processedHealth = currentHealth - (i*2);
            if (processedHealth >= 2) {
                hearts[i].sprite = fullHeartSprite;
            } else if (processedHealth == 1) {
                hearts[i].sprite = halfHeartSprite;
            } else if (processedHealth <= 0) {
                hearts[i].sprite = emptyHeartSprite;
            }
            hearts[i].gameObject.SetActive(UpgradeManager.instance.maxHealth/2 >= i+1);
        }
        UpdateCommandsState();
        beatsText.text = "BEATS  " + GameManager.instance.beatsFinished.ToString();
    }

    public void UpdateCommandsState() {
        for (int i=0; i< BeatManager.instance.beatPattern.Length; i++) {
            var active = BeatManager.instance.currentBeat == i && StateManager.instance.gameState == GameState.Beating;
            var type = BeatManager.instance.beatPattern[i];
            playerCommands[i].Setup(type == 0 ? 
                BeatManager.instance.playerCommand0.beatType : 
                BeatManager.instance.playerCommand1.beatType, 
                active, 
                type);
            enemyCommands[i].Setup(type == 0 ? 
                BeatManager.instance.enemyBeat0 : 
                BeatManager.instance.enemyBeat1, 
                active,
                type);
        }
    }
}
