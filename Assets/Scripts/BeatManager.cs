﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum BeatType {
    Empty,
    Move,
    Attack,
    EnemyAttack,
    GetCoins,
    Heal,
    Spawn,
    GetAmmo
}
public class BeatManager : MonoBehaviour {
    public static BeatManager instance;
    public int[] beatPattern = {0,0,1,0,0,1,0,1};
    public List<int[]> beatPatterns = new List<int[]>{new int[]{0,0,1,0,0,1,0,1},new int[]{1,1,1,1,0,0,0,0},new int[]{1,1,1,0,1,1,1,0},new int[]{0,1,0,1,0,1,0,1}};
    public int[] activeNotes = {1,0,1,1,0,1,1,0,1,1,0,1};
    public float[] beatTimes = {1,1,1,1,1,1,1,1};
    public Command playerCommand0;
    public Command playerCommand1;
    public BeatType enemyBeat0;
    public BeatType enemyBeat1;
    public UnityEvent onBeatFinished = new UnityEvent();
    private int beatCount = 3;
    bool waiting = true;
    int speedDelimeter = 8;
    public int currentBeat = -1;

    private void Awake() {
        BeatManager.instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        EndBeating();
        float time = 0;
        var count = 0;
        for (int i=0; i<activeNotes.Length; i++) {
            if (activeNotes[i] == 1) {
                beatTimes[count] = time;
                count++;
            }
            time += AudioManager.instance.trackLength/speedDelimeter/(float)activeNotes.Length;
        }
    }

    public void StartBeating() {
        currentBeat = -1;
        waiting = true;
        beatCount = 2;
    }

    public void EndBeating() {
        beatPattern = beatPatterns[Random.Range(0, beatPatterns.Count)];
        var enemyBeats = new List<BeatType>(new BeatType[]{BeatType.Move, BeatType.Spawn});
        enemyBeat0 = enemyBeats[Random.Range(0, enemyBeats.Count)];
        enemyBeats.Remove(enemyBeat0);
        enemyBeat1 = enemyBeats[Random.Range(0, enemyBeats.Count)];
    }

    // Update is called once per frame
    void FixedUpdate() {
        var chordsTimer = AudioManager.instance.chordsSources[0].time % (AudioManager.instance.trackLength/speedDelimeter);
        Debug.Log(chordsTimer);
        var chordsBeat = currentBeat;
        for (int i = 0; i < beatTimes.Length; i++) {
            if (chordsTimer > beatTimes[i]) {
                chordsBeat = i;
            }
        }
        if (StateManager.instance.gameState == GameState.Beating) {
            if (waiting) {
                if (chordsTimer >= beatTimes[0] && chordsTimer < beatTimes[1]) {
                    waiting = false;
                    currentBeat = -1;
                }
            } 
            if (!waiting) {
                if (chordsBeat != currentBeat) {
                    if (currentBeat == 7 && chordsBeat == 0) {
                        beatCount -= 1;
                        if (beatCount > 0) {
                            currentBeat = -1;
                        } else {
                            onBeatFinished.Invoke();
                            return;
                        }
                    }
                    currentBeat = chordsBeat;
                    UIManager.instance.UpdateCommandsState();
                    ProcessBeat();
                }
            }
        }
    }

    void ProcessBeat() {
        var playerBeatType = beatPattern[currentBeat] == 0 ? playerCommand0.beatType : playerCommand1.beatType;
        var enemyBeatType = beatPattern[currentBeat] == 0 ? enemyBeat0 : enemyBeat1;
        AudioManager.instance.Beat(enemyBeatType, playerBeatType);
        GameManager.instance.player.Beat(playerBeatType);
        GameManager.instance.Beat();
        foreach (GameObject enemyObj in GameManager.instance.enemies) {
            enemyObj.GetComponent<Enemy>().Beat(enemyBeatType);
        }
        if (enemyBeatType == BeatType.Spawn) {
            Spawner.instance.Spawn();
        }
        if (playerBeatType == BeatType.GetCoins) {
            GameManager.instance.diamonds += 1;
        }
        foreach (Arrow arrow in GameObject.FindObjectsOfType<Arrow>()) {
            arrow.Beat();
        }
    }
}
