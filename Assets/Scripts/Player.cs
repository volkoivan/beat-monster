﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    public int arrowsCount = 20;
    public int health = 6;
    private Rigidbody2D rb;
    public GameObject arrowPrefab;
    public UnityEvent playerDied;
    // Start is called before the first frame update

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Beat(BeatType type) {
        switch (type)
        {
            case BeatType.Attack:
                if (arrowsCount >= UpgradeManager.instance.shotArrowsPerBeat && GameManager.instance.enemies.Count > 0) {
                    for (int i = 0; i<UpgradeManager.instance.shotArrowsPerBeat; i++) {
                        var direction = GameManager.instance.enemies[i].transform.position - transform.position;
                        arrowsCount -= UpgradeManager.instance.shotArrowsPerBeat;
                        var arrow = Instantiate(arrowPrefab, transform.parent);

                        arrow.transform.position = (Vector2)(transform.position);
                        var targetRotation = Quaternion.LookRotation(Vector3.forward, Quaternion.Euler(0, 0, 90) * direction);
                        arrow.transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 360);
                        arrow.transform.position += arrow.transform.right;
                    }
                }


                break;
            case BeatType.Heal:
                if (health < UpgradeManager.instance.maxHealth) {
                    this.health += 1;
                }
                break;
            case BeatType.GetAmmo:
                arrowsCount += UpgradeManager.instance.getAmmoPerBeat;
                break;
        }
    }

    public void DoDamage(Enemy enemy) {
        health -= enemy.damage;
        if (health <= 0) {
            playerDied.Invoke();
        }
    }
}
