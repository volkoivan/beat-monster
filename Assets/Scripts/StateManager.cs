﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum GameState {
    MainMenu,
    WaitingUserMove,
    Beating,
    GameOver
}
public class StateManager : MonoBehaviour
{
    static public StateManager instance;
    public GameState gameState = GameState.MainMenu;
    [SerializeField]
    private GameObject[] waitingUserMoveObjects;
    [SerializeField]
    private GameObject[] gameOverObjects;
    [SerializeField]
    private GameObject[] mainMenuObjects;

    private void Awake() {
        StateManager.instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        BeatManager.instance.onBeatFinished.AddListener(EndBeat);
        UpdateGameState();
    }

    void EndBeat() {
        StateManager.instance.gameState = GameState.WaitingUserMove;
        UpdateGameState();
        BeatManager.instance.EndBeating();
    }

    public void StartBeat() {
        BeatManager.instance.StartBeating();
        StateManager.instance.gameState = GameState.Beating;
        UpdateGameState();
    }

    public void StartGame() {
        StateManager.instance.gameState = GameState.WaitingUserMove;
        UpdateGameState();
    }

    public void EndGame() {
        StateManager.instance.gameState = GameState.MainMenu;
        UpdateGameState();
    }
    void UpdateGameState() {
        foreach (var gameObj in waitingUserMoveObjects) {
            gameObj.SetActive(gameState == GameState.WaitingUserMove);
        }
        foreach (var gameObj in mainMenuObjects) {
            gameObj.SetActive(gameState == GameState.MainMenu);
        }
        foreach (var gameObj in gameOverObjects) {
            gameObj.SetActive(gameState == GameState.GameOver);
        }
        UIManager.instance.UpdateUpgrades();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
