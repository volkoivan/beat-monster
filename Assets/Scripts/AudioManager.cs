﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public float trackLength = 0f;
    private AudioLowPassFilter lowPassFilter;
    public static AudioManager instance;
    [SerializeField]
    AudioClip[] riffClips;
    public List<AudioSource> riffSources = new List<AudioSource>();
    [SerializeField]
    AudioClip[] chordsClips;
    public List<AudioSource> chordsSources = new List<AudioSource>();
    [SerializeField]
    AudioSource clicksSource;
    [SerializeField]
    AudioSource playerSoundsSource;
    [SerializeField]
    AudioSource hitHatSource;
    [SerializeField]
    AudioSource enemySoundsSource;
    [SerializeField]
    AudioClip[] clickSounds;
    [SerializeField]
    AudioClip enemyAttack;
    [SerializeField]
    AudioClip enemySpawn;
    [SerializeField]
    AudioClip enemyMove;
    [SerializeField]
    AudioClip playerAttack;
    [SerializeField]
    AudioClip playerHeal;
    [SerializeField]
    AudioClip playerAmmo;
    [SerializeField]
    AudioClip playerDiamonds;
    [SerializeField]
    AudioClip hitHat;
    float riffTimer = 0f;
    int currentRiff = 0;

    private void Awake() {
        AudioManager.instance = this;
        lowPassFilter = GameObject.FindObjectOfType<AudioLowPassFilter>().GetComponent<AudioLowPassFilter>();
    }
    // Start is called before the first frame update
    void Start()
    {
        foreach (AudioClip clip in chordsClips) {
            var source = gameObject.AddComponent<AudioSource>();
            chordsSources.Add(source);
            source.volume = 0;
            clip.LoadAudioData();
            source.clip = clip;
            source.loop = true;
            source.Play();
        }
        foreach (AudioClip clip in riffClips) {
            var source = gameObject.AddComponent<AudioSource>();
            riffSources.Add(source);
            source.volume = 0;
            clip.LoadAudioData();
            source.clip = clip;
            source.loop = true;
            source.Play();
        }
        foreach (AudioSource source in chordsSources) {
            source.timeSamples = chordsSources[0].timeSamples;
        }
        foreach (AudioSource source in riffSources) {
            source.timeSamples = chordsSources[0].timeSamples;
        }
        trackLength = chordsClips[0].length;
        chordsSources[0].volume = 1;
    }

    public void Beat(BeatType enemyBeat, BeatType playerBeat) {
        AudioClip playerClip = null;
        AudioClip enemyClip = null;
        switch (enemyBeat)
        {
            case BeatType.Spawn:
                enemyClip = enemySpawn;
                break;
            case BeatType.EnemyAttack:
                enemyClip = enemyAttack;
                break;
            case BeatType.Move:
                enemyClip = enemyMove;
                break;
        }
        switch (playerBeat)
        {
            case BeatType.GetAmmo:
                playerClip = playerAmmo;
                break;
            case BeatType.GetCoins:
                playerClip = playerDiamonds;
                break;
            case BeatType.Attack:
                playerClip = playerAttack;
                break;
            case BeatType.Heal:
                playerClip = playerHeal;
                break;
        }
        if (playerClip != null) {
            playerSoundsSource.volume = Random.Range(0.5f, 0.6f);
            playerSoundsSource.PlayOneShot(playerClip);
        }
        if (enemyClip != null) {
            enemySoundsSource.volume = Random.Range(0.5f, 0.6f);
            enemySoundsSource.PlayOneShot(enemyClip);
        }
        hitHatSource.volume = Random.Range(0.4f, 0.6f);
        hitHatSource.pitch = Random.Range(0.99f, 1.01f);
        hitHatSource.PlayOneShot(hitHat);
    }

    public void UIClick() {
        clicksSource.clip = clickSounds[Random.Range(0, clickSounds.Length)];
        clicksSource.pitch = Random.Range(0.9f, 1.1f);
        clicksSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        lowPassFilter.cutoffFrequency += Time.deltaTime * 4000 * (StateManager.instance.gameState == GameState.Beating ? 1 : -1);
        lowPassFilter.cutoffFrequency = Mathf.Clamp(lowPassFilter.cutoffFrequency, 400, 4000);
        var index = Mathf.Min(chordsSources.Count-1, GameManager.instance.level);
        for (int i=0; i < chordsSources.Count; i++) {

            if (i == index) {
                chordsSources[i].volume += Time.deltaTime;
            } else {
                chordsSources[i].volume -= Time.deltaTime;
            }
            chordsSources[i].volume = Mathf.Clamp(chordsSources[i].volume, 0, 1);
        }
        riffTimer += Time.deltaTime;
        if (riffTimer > riffClips[0].length) {
            riffTimer = 0f;
            currentRiff = Random.Range(0, riffClips.Length);
        }
        for (int i=0; i < riffSources.Count; i++) {
            if (i == currentRiff) {
                riffSources[i].volume += Time.deltaTime*2;
                riffSources[i].volume = Mathf.Clamp(riffSources[i].volume, 0, StateManager.instance.gameState == GameState.Beating ? 0.5f : 1);
            } else {
                riffSources[i].volume -= Time.deltaTime*2;
            }
        }
    }
}
