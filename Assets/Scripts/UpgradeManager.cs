﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeManager : MonoBehaviour
{
    static public UpgradeManager instance;
    public int shotArrowsPerBeat = 1;
    public int maxHealth = 6;
    public int getAmmoPerBeat = 1;
    public int bowLevel = 0;
    public int healthLevel = 0;
    public int arrowsLevel = 0;
    public int maxLevel = 4;
    public int arrowsPrice = 10;
    public int healthPrice = 10;
    public int bowPrice = 20;

    private void Awake() {
        UpgradeManager.instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    public void Reset() {
        bowLevel = 0;
        healthLevel = 0;
        arrowsLevel = 0;
        UpdateValuesByUpgrades();
    }
    
    private void UpdateValuesByUpgrades() {
        shotArrowsPerBeat = 1+bowLevel;
        maxHealth = 6+healthLevel*2;
        getAmmoPerBeat = 1+arrowsLevel;

        arrowsPrice = 4 + arrowsLevel*2;
        bowPrice = 6 + bowLevel*2;
        healthPrice = 6 + healthLevel*2;
        UIManager.instance.UpdateUpgrades();
    }

    public void UpgradeArrows() {
        if (GameManager.instance.diamonds >= arrowsPrice) {
            GameManager.instance.diamonds -= arrowsPrice;
        }
        arrowsLevel += 1;
        UpdateValuesByUpgrades();
    }

    public void UpgradeHealth() {
        if (GameManager.instance.diamonds >= healthPrice) {
            GameManager.instance.diamonds -= healthPrice;
        }
        healthLevel += 1;
        UpdateValuesByUpgrades();

    }

    public void UpgradeBow() {
        if (GameManager.instance.diamonds >= bowPrice) {
            GameManager.instance.diamonds -= bowPrice;
        }
        bowLevel += 1;
        UpdateValuesByUpgrades();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
